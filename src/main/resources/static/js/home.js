function renderPost(resume, place) {
    let innerHtml = '';

    for (let i = 0; i < resume.length; i++) {
        innerHtml += '<div class="row">'
        innerHtml +=    '<div class="col-12">'
        innerHtml +=        '<div class="main-resume">'
        innerHtml +=            '<div class="main-post__head">'
        innerHtml +=                '<h3>' + resume[i].title + '</h3>'
        innerHtml +=            '</div>'
        innerHtml +=             '<div class="main-post__middle">'
        innerHtml +=                 '<p>' + resume[i].position + '</p>'
        innerHtml +=             '</div>'
        innerHtml +=             '<div class="main-post__foot">'
        innerHtml +=                 '<div class="left">'
        innerHtml +=                     '<button type="button" class="btn">'
        innerHtml +=                         '<svg xmlns="http://www.w3.org/2000/svg" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" class="bi bi-trash-fill">'
        innerHtml +=                             '<path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>'
        innerHtml +=                         '</svg>'
        innerHtml +=                     '</button>'
        innerHtml +=                     '<span id="delete' + resume[i].id + '"></span>'
        innerHtml +=                 '</div>'
        innerHtml +=                 '<div class="right">'
        innerHtml +=                     '<form method="get" action="/redact">'
        innerHtml +=                         '<input type="hidden" name="post_id" value="' + resume[i].id + '">'
        innerHtml +=                         '<button type="submit" class="button"><span>Редактировать </span></button>'
        innerHtml +=                     '</form>'
        innerHtml +=                 '</div>'
        innerHtml +=             '</div>'
        innerHtml +=         '</div>'
        innerHtml +=     '</div>'
        innerHtml += '</div>'
    }

    place.html(innerHtml);
}