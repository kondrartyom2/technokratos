package my.technokratos.resumeapp.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResumeForm {
    private String id;
    private String title;
    private String creatorFirstName;
    private String creatorLastName;
    private String creatorPatronymic;
    private String creatorBirthDate;
    private String creatorCity;
    private String creatorEmail;
    private String creatorPhone;
    private String creatorInformation;
    private String creatorSkills;
    private String creatorLanguages;
    private String creatorQualities;
}
