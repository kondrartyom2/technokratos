package my.technokratos.resumeapp.repositories;

import my.technokratos.resumeapp.models.Creator;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CreatorRepository extends MongoRepository<Creator, String> {
}
