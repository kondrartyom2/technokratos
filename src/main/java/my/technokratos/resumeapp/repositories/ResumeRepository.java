package my.technokratos.resumeapp.repositories;

import my.technokratos.resumeapp.dto.ResumeDto;
import my.technokratos.resumeapp.models.Resume;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface ResumeRepository extends MongoRepository<Resume, String> {
    public Resume getResumeBy(String id);
}
