package my.technokratos.resumeapp.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "creator")
public class Creator {
    @Id
    private String _id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private LocalDate birthDate;
    private String city;
    private String email;
    private String phone;
    private String information;
    private String skills;
    private String languages;
    private String qualities;
}
