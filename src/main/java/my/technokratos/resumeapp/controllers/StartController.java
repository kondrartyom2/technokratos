package my.technokratos.resumeapp.controllers;

import my.technokratos.resumeapp.dto.ResumeDto;
import my.technokratos.resumeapp.services.intr.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@Controller
public class StartController {

    @Autowired
    private ResumeService resumeService;

    @GetMapping("/start")
    public String getStartPage(Model model) {
        model.addAttribute("resumes", resumeService.getAllResumes());
        return "ftlh/start_page";
    }

    @GetMapping("/start/{resume-id}")
    public String getUsers(@PathVariable("resume-id") String id, Model model) {
        List<ResumeDto> resumes = new ArrayList<>();
        resumes.add(resumeService.getResumeById(id));
        model.addAttribute("resumes", resumes);
        return "ftlh/start_page";
    }

    @GetMapping("/start/delete/{resume-id}")
    public String deleteResume(@PathVariable("resume-id") String id) {
        resumeService.deleteResume(id);
        return "redirect:/start";
    }
}
