package my.technokratos.resumeapp.controllers;

import my.technokratos.resumeapp.dto.ResumeDto;
import my.technokratos.resumeapp.forms.ResumeForm;
import my.technokratos.resumeapp.models.Creator;
import my.technokratos.resumeapp.models.Resume;
import my.technokratos.resumeapp.services.intr.CreatorService;
import my.technokratos.resumeapp.services.intr.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDate;

@Controller
public class EditController {

    @Autowired
    private ResumeService resumeService;

    @Autowired
    private CreatorService creatorService;

    @GetMapping("/redact/{resume-id}")
    public String getAddResumePage(@PathVariable("resume-id") String id, Model model) {
        ResumeDto resume = resumeService.getResumeById(id);
        model.addAttribute("resume", resume);
        return "ftlh/edit_resume";
    }

    @PostMapping("/redact")
    public String postAddResumePage(ResumeForm form) {
        resumeService.updateResume(form);
        return "redirect:/start";
    }
}
