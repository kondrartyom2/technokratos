package my.technokratos.resumeapp.controllers;

import my.technokratos.resumeapp.forms.ResumeForm;
import my.technokratos.resumeapp.models.Creator;
import my.technokratos.resumeapp.models.Resume;
import my.technokratos.resumeapp.services.intr.CreatorService;
import my.technokratos.resumeapp.services.intr.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDate;

@Controller
public class AddResumeController {

    @Autowired
    private ResumeService resumeService;

    @GetMapping("/add_resume")
    public String getAddResumePage(Model model) {
        return "ftlh/resume_add";
    }

    @PostMapping("/add_resume")
    public String postAddResumePage(ResumeForm form) {
        resumeService.addResume(form);
        return "redirect:/start";
    }
}
