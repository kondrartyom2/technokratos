package my.technokratos.resumeapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import my.technokratos.resumeapp.models.Creator;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreatorDto {
    private String id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private LocalDate birthDate;
    private String city;
    private String email;
    private String phone;
    private String information;
    private String skills;
    private String languages;
    private String qualities;

    public static CreatorDto from(Creator creator) {
        return CreatorDto.builder()
                .id(creator.get_id())
                .firstName(creator.getFirstName())
                .lastName(creator.getLastName())
                .patronymic(creator.getPatronymic())
                .birthDate(creator.getBirthDate())
                .city(creator.getCity())
                .email(creator.getEmail())
                .phone(creator.getPhone())
                .information(creator.getInformation())
                .skills(creator.getSkills())
                .languages(creator.getLanguages())
                .qualities(creator.getQualities())
                .build();
    }

    public static List<CreatorDto> from(List<Creator> resumes) {
        return resumes.stream().map(CreatorDto::from).collect(Collectors.toList());
    }
}
