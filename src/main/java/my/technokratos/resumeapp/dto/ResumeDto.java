package my.technokratos.resumeapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import my.technokratos.resumeapp.models.Creator;
import my.technokratos.resumeapp.models.Resume;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResumeDto {
    private String id;
    private Creator creator;
    private String title;

    public static ResumeDto from(Resume resume) {
        return ResumeDto.builder()
                .id(resume.get_id())
                .title(resume.getTitle())
                .creator(resume.getCreator())
                .build();
    }

    public static List<ResumeDto> from(List<Resume> resumes) {
        return resumes.stream().map(ResumeDto::from).collect(Collectors.toList());
    }
}
