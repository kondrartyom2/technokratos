package my.technokratos.resumeapp.services.intr;

import my.technokratos.resumeapp.dto.CreatorDto;
import my.technokratos.resumeapp.models.Creator;

import java.util.List;

public interface CreatorService {
    public List<CreatorDto> getAllCreators();
    public CreatorDto getCreatorById(String id);
    public Creator addCreator(CreatorDto creatorDto);

    Creator updateCreator(Creator creator);

    Creator addCreator(Creator creator);

    void deleteCreatorById(String id);
}
