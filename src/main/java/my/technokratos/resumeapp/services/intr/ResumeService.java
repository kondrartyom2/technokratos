package my.technokratos.resumeapp.services.intr;

import my.technokratos.resumeapp.dto.ResumeDto;
import my.technokratos.resumeapp.forms.ResumeForm;
import my.technokratos.resumeapp.models.Resume;

import java.util.List;

public interface ResumeService {
    public List<ResumeDto> getAllResumes();
    public ResumeDto getResumeById(String id);
    public Resume addResume(ResumeForm resumeForm);
    public void deleteResume(String id);
    public void updateResume(ResumeForm resumeForm);
}
