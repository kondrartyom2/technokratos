package my.technokratos.resumeapp.services.impl;

import my.technokratos.resumeapp.dto.CreatorDto;
import my.technokratos.resumeapp.models.Creator;
import my.technokratos.resumeapp.repositories.CreatorRepository;
import my.technokratos.resumeapp.services.intr.CreatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

import static my.technokratos.resumeapp.dto.CreatorDto.from;

@Component
public class CreatorServiceImpl implements CreatorService {

    @Autowired
    CreatorRepository creatorRepository;

    @Override
    public List<CreatorDto> getAllCreators() {
        return from(creatorRepository.findAll());
    }

    @Override
    public CreatorDto getCreatorById(String id) {
        return from(creatorRepository.findById(id).orElse(Creator.builder().build()));
    }

    @Override
    public Creator addCreator(CreatorDto creatorDto) {
        return creatorRepository.save(
                Creator.builder()
                        .firstName(creatorDto.getFirstName())
                        .lastName(creatorDto.getLastName())
                        .birthDate(creatorDto.getBirthDate())
                        .email(creatorDto.getEmail())
                        .phone(creatorDto.getPhone())
                        .city(creatorDto.getCity())
                        .skills(creatorDto.getSkills())
                        .languages(creatorDto.getLanguages())
                        .information(creatorDto.getInformation())
                        .qualities(creatorDto.getQualities())
                        .build()
        );
    }

    @Override
    public Creator updateCreator(Creator creator) {
        return creatorRepository.save(creator);
    }

    @Override
    public Creator addCreator(Creator creator) {
        return creatorRepository.save(creator);
    }

    @Override
    public void deleteCreatorById(String id) {
        creatorRepository.deleteById(id);
    }
}
