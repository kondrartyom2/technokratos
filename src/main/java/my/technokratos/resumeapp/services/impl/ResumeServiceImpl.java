package my.technokratos.resumeapp.services.impl;

import my.technokratos.resumeapp.dto.CreatorDto;
import my.technokratos.resumeapp.dto.ResumeDto;
import my.technokratos.resumeapp.forms.ResumeForm;
import my.technokratos.resumeapp.models.Creator;
import my.technokratos.resumeapp.models.Resume;
import my.technokratos.resumeapp.repositories.ResumeRepository;
import my.technokratos.resumeapp.services.intr.CreatorService;
import my.technokratos.resumeapp.services.intr.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

import static my.technokratos.resumeapp.dto.ResumeDto.from;

@Component
public class ResumeServiceImpl implements ResumeService {

    @Autowired
    private ResumeRepository resumeRepository;

    @Autowired
    private CreatorService creatorService;

    @Override
    public List<ResumeDto> getAllResumes() {
        return from(resumeRepository.findAll());
    }

    @Override
    public ResumeDto getResumeById(String id) {
        return from(resumeRepository.findById(id).orElse(
                Resume.builder()
                .build()
        ));
    }

    @Override
    public Resume addResume(ResumeForm resumeForm) {
        Creator creator = creatorService.addCreator(
                Creator.builder()
                        .firstName(resumeForm.getCreatorFirstName())
                        .lastName(resumeForm.getCreatorLastName())
                        .birthDate(LocalDate.parse(resumeForm.getCreatorBirthDate()))
                        .email(resumeForm.getCreatorEmail())
                        .phone(resumeForm.getCreatorPhone())
                        .city(resumeForm.getCreatorCity())
                        .skills(resumeForm.getCreatorSkills())
                        .languages(resumeForm.getCreatorLanguages())
                        .information(resumeForm.getCreatorInformation())
                        .qualities(resumeForm.getCreatorQualities())
                        .build()
        );

        return resumeRepository.save(
                Resume.builder()
                        .title(resumeForm.getTitle())
                        .creator(creator)
                        .build()
        );
    }

    @Override
    public void deleteResume(String id) {
        ResumeDto resumeDto = from(resumeRepository.getResumeBy(id));
        creatorService.deleteCreatorById(resumeDto.getCreator().get_id());
        resumeRepository.deleteById(id);
    }

    @Override
    public void updateResume(ResumeForm resumeForm) {
        CreatorDto creatorDto = creatorService.getCreatorById(
                getResumeById(resumeForm.getId()).getCreator().get_id()
        );

        Creator creator = creatorService.updateCreator(
                Creator.builder()
                        ._id(creatorDto.getId())
                        .firstName(resumeForm.getCreatorFirstName())
                        .lastName(resumeForm.getCreatorLastName())
                        .birthDate(LocalDate.parse(resumeForm.getCreatorBirthDate()))
                        .email(resumeForm.getCreatorEmail())
                        .phone(resumeForm.getCreatorPhone())
                        .city(resumeForm.getCreatorCity())
                        .skills(resumeForm.getCreatorSkills())
                        .languages(resumeForm.getCreatorLanguages())
                        .information(resumeForm.getCreatorInformation())
                        .qualities(resumeForm.getCreatorQualities())
                        .build()
        );
        resumeRepository.save(
                Resume.builder()
                        ._id(resumeForm.getId())
                        .creator(creator)
                        .title(resumeForm.getTitle())
                        .build()
        );
    }
}
